var express = require('express');
var app = express();

app.get('/', function (req, res) {
   console.log("Get request from root");
   res.writeHead(200, {'Content-Type': 'text/plain'});
   res.end('Hello World from John Zen');
})

app.use('/healthcheck', require('express-healthcheck')({
   healthy: function () {
       console.log("Healthcheck");
       return { uptime: process.uptime()};
   }
}));

app.get('/meta', function (req, res) {
   var fs = require("fs");
   console.log("loading package.json");
   var content = fs.readFileSync("package.json");
   var jsonContent = JSON.parse(content);
   var applicationName = jsonContent.name;
   var applicationVersion = jsonContent.version;
   var applicationDescription = jsonContent.description;
   var applicationLastCommitSha = jsonContent.lastcommitsha;
   if (typeof(applicationName) == "undefined")
       applicationceName = "";
   if (typeof(applicationVersion) == "undefined")
       applicationceVersion = "";
   if (typeof(applicationDescription) == "undefined")
       applicationceDescription = "";
   if (typeof(applicationLastCommitSha) == "undefined")
       applicationceLastCommitSha = "";

   var outputContentJson = {
	"version": applicationVersion,
        "description": applicationDescription,
        "lastcommitsha": applicationLastCommitSha
   }
   var jsonString = JSON.stringify(outputContentJson, null, 4); 
   var outputString = '"' + applicationName + '": [\n' + jsonString + "\n] \n";
   console.log("Json is:" + outputString);
   console.log("Get metadata");
   res.writeHead(200, {'Content-Type': 'application/json'});
   res.end(outputString);
})
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Sample app for John Zen listening at http://%s:%s", host, port)
})
