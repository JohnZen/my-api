# MY-APP #

A simple app to demonstrate continuous deployment.

## Deployment
The final deployment is using docker. The docker images is hosted on hub.docker.com.

To deploy, run the following in the deploy machine
```
docker pull johnzen/my-app:<version>
docker run -p 8081:8081 johnzen/my-app:<version>
```

Browse or curl the following URL
```
#hello world api
http://<host-machine>:8081/
#healthcheck
http://<host-machine>:8081/healthcheck
#metadata
http://<host-machine>:8081/meta
```

## Deploy Build
Deploy build is as follow:

bitbucket --on change--> pipelines build --push to--> docker hub

During pipelines build:

- version and commit sha are set in the build (not commited),
- docker image built
- docker image pushed to docker hub

## Tools used
Deploy mechanism: docker

Continuous Integration / Deployment: bitbucket pipelines

source control: bitbucket

programming language: javascript

libraries: nodejs, express, express-healthcheck, frisby, jest,
